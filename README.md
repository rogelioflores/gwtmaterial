# NetIQ GWT Material Module

This GWT module is intended to be used as a theme for any other GWT module or GWT-based app.

##Highlights
* Use of flat styles that mimic Material Design
* Support for CSS3 through GSS (Google Closure Stylesheets)
* Uses maven for building, dependency-management, running, etc.

##This module/app was generated using an archetype in this manner*:

```
mvn archetype:generate \
-DarchetypeRepository=https://oss.sonatype.org/content/repositories/snapshots \
-DarchetypeGroupId=com.github.branflake2267.archetypes \
-DarchetypeArtifactId=gwt-basic-archetype \
-DarchetypeVersion=1.6-SNAPSHOT \
-DgroupId=com.netiq.gwtmaterial \
-DartifactId=gwt-material \
-Dmodule=GwtMaterial
```

* The archetype above used GWT 2.6.1 and Java 6 by default at the time of creation (Tue 16 Dec 2014 10:42:09 AM EST), but project was updated to use GWT 2.7.0 and Java 7.
