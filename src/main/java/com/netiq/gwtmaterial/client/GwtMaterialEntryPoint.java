package com.netiq.gwtmaterial.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.dom.client.Style;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;

import com.netiq.gwtmaterial.client.resources.CoreClientBundle;
import com.netiq.gwtmaterial.client.resources.MaterialResources;
import com.netiq.gwtmaterial.client.widgets.PaperSlider;
import com.netiq.gwtmaterial.client.widgets.WidgetsPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class GwtMaterialEntryPoint implements EntryPoint {

   private final MaterialResources.MaterialStyle css = CoreClientBundle.INSTANCE.materialStyle();

   @Override
   public void onModuleLoad() {
      css.ensureInjected();

      HTML header = new HTML("<h1>NetIQ Material</h1>");
      header.setStyleName(css.header());

      HTML headerWithPadding = new HTML("<h4>Header with padding</h4>");
      headerWithPadding.setStyleName(css.headerWithPadding());

//    NetiqButton button = new NetiqButton("Click Me");
//    SimplePanel p = new SimplePanel();
//    p.getElement().appendChild(button.rootElement);

      HTML headers = new HTML("<h1>Heading One</h1>" +
              "<h2>Heading Two</h2>" +
              "<h3>Heading Three</h3>" +
              "<h4>Heading Four</h4>" +
              "<h5>Heading Five</h5>" +
              "<h6>Heading Six</h6>");
      SimplePanel sp = new SimplePanel();
      sp.setStyleName(css.cardPanel());
      sp.add(headers);

      Label userAgent = new Label("ua: "+ Window.Navigator.getUserAgent().toLowerCase());
      Label platform = new Label("platform: "+ Window.Navigator.getPlatform().toLowerCase());
      Label isChrome = new Label("isChrome: "+ isChromeBrowser());
      Label isFirefox = new Label("isFF: " + isFirefoxBrowser());
      Label isIE = new Label("isIE: " + isIEBrowser());

      VerticalPanel vp = new VerticalPanel();
      vp.setSpacing(16);
      vp.add(headerWithPadding);
      vp.add(sp);
      vp.add(userAgent);
      vp.add(platform);
      vp.add(isChrome);
      vp.add(isFirefox);
      vp.add(isIE);

      DockLayoutPanel mainPanel = new DockLayoutPanel(Style.Unit.PX);
      mainPanel.addNorth(header, 140);
      mainPanel.addWest(new HTML(""), 16);
      mainPanel.addEast(new WidgetsPanel(), 650);
      mainPanel.add(vp);

      RootLayoutPanel.get().add(mainPanel);
   }

   private boolean isChromeBrowser() {
      return Window.Navigator.getUserAgent().toLowerCase().contains( "chrome" ) &&
              !Window.Navigator.getUserAgent().toLowerCase().contains( "mobile" ) &&
              !Window.Navigator.getPlatform ().toLowerCase().contains( "mobile" );
   }

   private boolean isFirefoxBrowser() {
      return Window.Navigator.getUserAgent().toLowerCase().contains("firefox") &&
              !Window.Navigator.getUserAgent().toLowerCase().contains("mobile") &&
              !Window.Navigator.getPlatform().toLowerCase().contains("mobile");
   }

   private boolean isIEBrowser() {
      return Window.Navigator.getUserAgent().toLowerCase().contains("explorer") &&
              !Window.Navigator.getUserAgent().toLowerCase().contains("mobile") &&
              !Window.Navigator.getPlatform().toLowerCase().contains("mobile");
   }
}
