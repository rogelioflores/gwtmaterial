package com.netiq.gwtmaterial.client.widgets;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.EventListener;
import com.google.gwt.user.client.Window;

/**
 * Created by rflores on 12/18/14.
 */
public class NetiqButton implements EventListener {

   interface NetiqButtonUiBinder extends UiBinder<DivElement, NetiqButton> {   }
   private static NetiqButtonUiBinder ourUiBinder = GWT.create(NetiqButtonUiBinder.class);

   @UiField
   SpanElement text;
   DivElement rootElement;

   public NetiqButton(String buttonText) {

      text.setInnerText(buttonText);
      rootElement = ourUiBinder.createAndBindUi(this);
   }

   @Override
   public void onBrowserEvent(Event event) {
      Window.alert("yahoo!!");
   }

}
