package com.netiq.gwtmaterial.client.widgets;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiFactory;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.*;
import com.netiq.gwtmaterial.client.resources.MaterialResources;

/**
 * Created by rflores on 2/5/15.
 */
public class WidgetsPanel extends Composite {

   interface Binder extends UiBinder<HTMLPanel, WidgetsPanel> { }

   private static Binder ourUiBinder = GWT.create(Binder.class);

   @UiField
   MaterialResources.MaterialStyle css;

   @UiField(provided = true)
   MaterialButton materialButton;

   @UiField(provided = true)
   MaterialButton materialButtonFlat;

   @UiField
   MaterialCheckBox materialCb1;

   @UiField
   MaterialCheckBox materialCb2;

   @UiField(provided = true)
   PaperSlider paperSlider;

   public WidgetsPanel() {

      materialButton = new MaterialButton("button");
      materialButtonFlat = new MaterialButton("click me");
      paperSlider = new PaperSlider();
      paperSlider.setData("0", "1000", "900");

      initWidget(ourUiBinder.createAndBindUi(this));

      materialButtonFlat.addStyleName(css.ink());
      materialCb1.setText("This checkbox enables/disables the one below");
      materialCb2.setText("This is our cool-looking checkbox");
   }

   @UiFactory
   MaterialCheckBox makeMaterialCheckBox() {
      return new MaterialCheckBox("");
   }

   @UiHandler("materialButtonFlat")
   void onFlatClick(ClickEvent event) {
      HTML helloHtml = new HTML("<h5>Hola Mundo!</h5>");
      helloHtml.setStyleName(css.hello());

      DockLayoutPanel p = new DockLayoutPanel(Style.Unit.PX);
      p.add(helloHtml);
      p.setStyleName(css.popup());
      PopupPanel popupPanel = new PopupPanel(true);
      popupPanel.setAnimationEnabled(true);
      popupPanel.setAnimationType(PopupPanel.AnimationType.ROLL_DOWN);
      popupPanel.add(p);
      popupPanel.center();
   }

   @UiHandler("materialCb1")
   void onCheckBoxClicked(ClickEvent event) {
      materialCb2.setEnabled(!materialCb2.isEnabled());
   }

}
