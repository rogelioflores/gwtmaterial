package com.netiq.gwtmaterial.client.widgets;

import com.google.gwt.dom.client.InputElement;
import com.google.gwt.user.client.ui.CheckBox;
import com.netiq.gwtmaterial.client.resources.CoreClientBundle;
import com.netiq.gwtmaterial.client.resources.MaterialResources;

/**
 * Created by rflores on 12/19/14.
 */
public class MaterialCheckBox extends CheckBox {

   MaterialResources.MaterialStyle css = CoreClientBundle.INSTANCE.materialStyle();

   public MaterialCheckBox(String label) {
      super(label);
      setMaterialStyle();
   }

   private void setMaterialStyle() {
      InputElement inputElement = ((InputElement)getElement().getElementsByTagName("INPUT").getItem(0));
      inputElement.setClassName(css.checkbox());
   }

   @Override
   public void setEnabled(boolean enabled) {
      super.setEnabled(enabled);

      if (enabled) {
         this.removeStyleName(css.checkboxDisabled());
      }
      else {
         this.addStyleName(css.checkboxDisabled());
      }

   }
}
