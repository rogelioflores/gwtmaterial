package com.netiq.gwtmaterial.client.widgets;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.Widget;
import com.netiq.gwtmaterial.client.resources.CoreClientBundle;

/**
 * Created by rflores on 2/5/15.
 */
public class PaperSlider extends Widget {

   private static boolean injected = false;
   private static CoreClientBundle clientBundle = CoreClientBundle.INSTANCE;

   private String min = "0";
   private String max = "100";
   private String value = "50";

   /**
    * Ctor.
    * Ensures needed html templates are added and injects a <paper-tabs></paper-tabs> element to the page.
    */
   public PaperSlider() {
      super();
      ensureHtmlImport();
      Element el = DOM.createElement("paper-slider");
      setElement(el);
//      setStyleName(clientBundle.materialStyle().paperSlider());
   }

   public  static void ensureHtmlImport() {
      if (!injected) {
         Element head = Document.get().getElementsByTagName("head").getItem(0);
         Element htmlImport = Document.get().createLinkElement();
         htmlImport.setAttribute("rel", "import");
         GWT.log("base for static files: "+ GWT.getModuleBaseForStaticFiles());
         GWT.log("moduleBaseUrl: "+ GWT.getModuleBaseURL());
         htmlImport.setAttribute("href", "http://127.0.0.1:8888/components/bower_components/paper-slider/paper-slider.html");
         head.appendChild(htmlImport);
      }
   }

   public void setData(String min, String max, String value) {
      this.min = min;
      this.max = max;
      this.value = value;
      refresh();
   }

   public void refresh() {
      getElement().setAttribute("min", min);
      getElement().setAttribute("max", max);
      getElement().setAttribute("value", value);
   }
}
