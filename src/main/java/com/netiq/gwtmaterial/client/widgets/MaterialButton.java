package com.netiq.gwtmaterial.client.widgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Label;
import com.netiq.gwtmaterial.client.resources.CoreClientBundle;
import com.netiq.gwtmaterial.client.resources.MaterialResources;

/**
 * Created by rflores on 12/18/14.
 */
public class MaterialButton extends Label {

   private final MaterialResources.MaterialStyle css = CoreClientBundle.INSTANCE.materialStyle();

   public MaterialButton(String text) {
      super(text);
      setStyleName(css.button());
      addClickHandler(new ClickHandler() {
         @Override
         public void onClick(ClickEvent event) {
            startWaveEffect();
         }
      });
   }

   private void startWaveEffect() {
      this.addStyleName(css.active());
      Timer timer = new Timer() {
         @Override
         public void run() {
            stopWaveEffect();
         }
      };
      timer.schedule(300);
   }

   private void stopWaveEffect() {
      this.removeStyleName(css.active());
   }
}
