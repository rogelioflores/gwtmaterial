package com.netiq.gwtmaterial.client.resources;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;

/**
 * Created by rflores on 12/18/14.
 */
public interface CoreClientBundle extends ClientBundle {

   public static final CoreClientBundle INSTANCE = GWT.create(CoreClientBundle.class);

   @Source("styles/material.gss")
   MaterialResources.MaterialStyle materialStyle();

   //@Source("images/icon.png")
   //@ImageOptions(repeatStyle = RepeatStyle.Both)
   //ImageResource icon();
}
