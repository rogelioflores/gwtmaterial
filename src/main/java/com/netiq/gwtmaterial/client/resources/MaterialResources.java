package com.netiq.gwtmaterial.client.resources;

import com.google.gwt.resources.client.CssResource;

/**
 * Created by rflores on 12/18/14.
 */
public interface MaterialResources {

   public interface MaterialStyle extends CssResource {
      String header();

      @ClassName("header-with-padding")
      String headerWithPadding();

      @CssResource.ClassName("material-btn")
      String button();

      String active();

      String ink();

      @ClassName("card-panel")
      String cardPanel();

      String popup();

      String hello();

      @ClassName("paper-slider")
      String paperSlider();

      @ClassName("material-cb")
      String checkbox();

      @ClassName("material-cb-disabled")
      String checkboxDisabled();
   }
}

// To use in UiBinder files:
//<ui:style type='com.netiq.gwtmaterial.client.resources.MaterialResources.MaterialStyle' gss="true">
